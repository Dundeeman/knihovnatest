<!DOCTYPE html>
<html>
<head>
    <title>Knihovna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <a href="popularita.php">Popularita autorů</a>
    <a href="administrace.php">Administrace</a>
    <br>
    <a href="autori.php">Autoři</a>
    <a href="knihy.php">Knihy</a>
    <a href="zakaznici.php">Zákazníci</a>
    <a href="vypujcky.php">Výpůjčky</a>

<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
    <input type="hidden" name="action" value="<?php echo htmlspecialchars($_GET['action']); ?>">
    ID : <input type="text" name="id" value="<?php echo htmlspecialchars($_GET['id']);?>" readonly><br>
    Jméno: <input type="text" name="jmeno" value="<?php echo htmlspecialchars($_GET['jmeno']);?>"><br>
    Příjmení: <input type="text" name="prijmeni" value="<?php echo htmlspecialchars($_GET['prijmeni']);?>"><br>
    Datum narození: <input type="date" name="narozeni" placeholder="YYYY-MM-DD" value="<?php echo htmlspecialchars($_GET['narozeni']);?>"><br>
  <input type="submit" value="Uložit">
</form>
<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
<input type="hidden" name="id" value="<?php echo htmlspecialchars($_GET['id']);?>" readonly><br>
<input type="hidden" name="action" value="delete">
<input type="submit" value="Smazat">
</form>
<?php
$dbconfig = parse_ini_file ('dbconfig.ini');
function delete()
{
    $connection = new mysqli($dbconfig['adress'],$dbconfig['usr'],$dbconfig['pwd'],$dbconfig['db']);
    $change = $connection->prepare('delete from autori where id=?');
    $change->bind_param('i', $_POST['id']);
    $change->execute();
    $connection->close();
}
function change()
{
    $connection = new mysqli($dbconfig['adress'],$dbconfig['usr'],$dbconfig['pwd'],$dbconfig['db']);
    $jmeno = htmlspecialchars($_POST['jmeno']);
    $prijmeni = htmlspecialchars($_POST['prijmeni']);
    $narozeni = htmlspecialchars($_POST['narozeni']);
    $id = htmlspecialchars($_POST['id']);
    if ($_POST['action']=='edit' && $id!=null) {
        $change = $connection->prepare('update autori set jmeno=?, prijmeni=?, narozeni=? where id=?');
    } elseif ($_POST['action']=='add') {
        $change = $connection->prepare('insert into autori (jmeno,prijmeni,narozeni,id) values (?,?,?,?)');
    }
    $change->bind_param('sssi', $jmeno, $prijmeni, $narozeni, $id);
    $change->execute();
    $connection->close();
}
if ($_SERVER["REQUEST_METHOD"] == 'POST') {
    if ($_POST['action']=='delete') {
        delete();
    } else {
        change();
    }
    echo '<script type="text/javascript">
    window.location = "autori.php"
</script>';
}
?>

</body>
</html>