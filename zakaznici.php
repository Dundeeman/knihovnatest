<!DOCTYPE html>
<html>

<head>
    <title>Knihovna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <a href="popularita.php">Popularita autorů</a>
    <a href="administrace.html">Administrace</a>
    <br>
    <a href="autori.php">Autoři</a>
    <a href="knihy.php">Knihy</a>
    <a href="zakaznici.php">Zákazníci</a>
    <a href="vypujcky.php">Výpůjčky</a>
    <?php
    $dbconfig = parse_ini_file ('dbconfig.ini');
    $connection = new mysqli($dbconfig['adress'], $dbconfig['usr'], $dbconfig['pwd'], $dbconfig['db']);
    $query = "SELECT * FROM `zakaznici`";
    $res = $connection->query($query);
    echoTable($res);
    $connection->close();
    function echoTable($res)
    {
        echo '<table cellpadding="3" cellspacing="1" border="1">';
        echo '<tr>';
        echo '<th scope="col">ID</th>';
        echo '<th scope="col">Jméno</th>';
        echo '<th scope="col">Příjmení</th>';
        echo '<th scope="col">Datum narození</th>';
        echo '<th scope="col">Email</th>';
        echo '<th scope="col"></th>';
        echo '</tr>';
        while ($row = $res->fetch_array()) {
            echo '<tr>';
            echo "<th scope='col'>{$row['id']}</th>";
            echo "<th scope='col'>{$row['jmeno']}</th>";
            echo "<th scope='col'>{$row['prijmeni']}</th>";
            echo "<th scope='col'>{$row['narozen']}</th>";
            echo "<th scope='col'>{$row['email']}</th>";
            echo "<th scope='col'><a href=\"upravitzakazniky.php?action=edit&id={$row['id']}&jmeno={$row['jmeno']}&prijmeni={$row['prijmeni']}&narozen={$row['narozen']}&email={$row['email']}\">Upravit</a></th>";  //Mozna lepsi posat jenom id a pak znovu nacist z databaze?
            echo '</tr>';
        }
        echo "</table>";
    }
?>
<a href="upravitautory.php?action=add">Pridat</a>    

</body>

</html>