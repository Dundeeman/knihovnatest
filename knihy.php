<!DOCTYPE html>
<html>

<head>
    <title>Knihovna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <a href="popularita.php">Popularita autorů</a>
    <a href="administrace.html">Administrace</a>
    <br>
    <a href="autori.php">Autoři</a>
    <a href="knihy.php">Knihy</a>
    <a href="zakaznici.php">Zákazníci</a>
    <a href="vypujcky.php">Výpůjčky</a>
    <?php
    $dbconfig = parse_ini_file ('dbconfig.ini');
    
$connection = new mysqli($dbconfig['adress'],$dbconfig['usr'],$dbconfig['pwd'],$dbconfig['db']);
$query = "SELECT GROUP_CONCAT(concat_ws(' ', autori.jmeno, autori.prijmeni) SEPARATOR ', ') as autor, knihy.id,knihy.nazev,knihy.rok_vydani from knihy left join autorstvi on knihy.id = autorstvi.kniha left join autori on autorstvi.autor = autori.id GROUP BY knihy.id;";
$res = $connection->query($query);
echo $connection->error;
printTable($res);
$connection->close();
function printTable($res)
{
    print '<table cellpadding="3" cellspacing="1" border="1">';
    print '<tr>';
    print '<th scope="col">ID</th>';
    print '<th scope="col">Název</th>';
    print '<th scope="col">Autor</th>';
    print '<th scope="col">Rok vydání</th>';
    print '<th scope="col"></th>';    
    print '</tr>';
    while ($row = $res->fetch_array())
    {
        print '<tr>';
        print "<th scope='col'>{$row['id']}</th>";
        print "<th scope='col'>{$row['nazev']}</th>";
        print "<th scope='col'>{$row['autor']}</th>";
        print "<th scope='col'>{$row['rok_vydani']}</th>";
        print "<th scope='col'><a href=\"upravitknihy.php?action=edit&id={$row['id']}&nazev={$row['nazev']}&autor={$row['autor']}&vydani={$row['rok_vydani']}\">Upravit</a></th>";  //Mozna lepsi posat jenom id a pak znovu nacist z databaze?              
        print '</tr>';
    }
    print "</table>";
}

?>
<a href="upravitknihy.php?action=add">Pridat</a>    


</body>

</html>