<!DOCTYPE html>
<html>

<head>
    <title>Knihovna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <a href="popularita.php">Popularita autorů</a>
    <a href="administrace.html">Administrace</a>
    <br>
    <a href="autori.php">Autoři</a>
    <a href="knihy.php">Knihy</a>
    <a href="zakaznici.php">Zákazníci</a>
    <a href="vypujcky.php">Výpůjčky</a>
    <?php
    $dbconfig = parse_ini_file ('dbconfig.ini');    
    $connection = new mysqli($dbconfig['adress'], $dbconfig['usr'], $dbconfig['pwd'], $dbconfig['db']);
    $query = "SELECT GROUP_CONCAT(concat_ws(' ', autori.jmeno, autori.prijmeni) SEPARATOR ', ') as autor, 
        vypujcky.kniha as idknihy, knihy.nazev as nazevknihy, knihy.rok_vydani as vydano,
        concat_ws(' ', zakaznici.jmeno, zakaznici.prijmeni) as zakaznik, zakaznici.id as idzakaznika, zakaznici.email as email,
        predpokladanevraceni, skutecnevraceni, pujceno,vypujcky.id as idvypujcky
        from vypujcky
        left join knihy on vypujcky.kniha = knihy.id 
        left join autorstvi on autorstvi.kniha = vypujcky.kniha
        left join autori on autorstvi.autor = autori.id
        left join zakaznici on vypujcky.zakaznik = zakaznici.id
        GROUP BY knihy.id,vypujcky.id;";
    $res = $connection->query($query);
    echo $connection->error;
    echoTable($res);
    $connection->close();
    function echoTable($res)
    {
        echo '<table cellpadding="3" cellspacing="1" border="1">';
        echo '<tr>';
        echo '<th scope="col">ID výpůjčky</th>';
        echo '<th scope="col">ID knihy</th>';
        echo '<th scope="col">Název knihy</th>';
        echo '<th scope="col">Autor</th>';
        echo '<th scope="col">Rok vydání</th>';
        echo '<th scope="col">ID zákazníka</th>';
        echo '<th scope="col">Zákazník</th>';
        echo '<th scope="col">Datum půjčení</th>';
        echo '<th scope="col">Předpokládané datum vrácení</th>';
        echo '<th scope="col">Datum vrácení</th>';
        echo '<th scope="col"></th>';
        echo '</tr>';
        while ($row = $res->fetch_array()) {
            echo '<tr>';
            echo "<th scope='col'>{$row['idvypujcky']}</th>";
            echo "<th scope='col'>{$row['idknihy']}</th>";
            echo "<th scope='col'>{$row['nazevknihy']}</th>";
            echo "<th scope='col'>{$row['autor']}</th>";
            echo "<th scope='col'>{$row['vydano']}</th>";
            echo "<th scope='col'>{$row['idzakaznika']}</th>";
            echo "<th scope='col'>{$row['zakaznik']}</th>";
            echo "<th scope='col'>{$row['pujceno']}</th>";
            echo "<th scope='col'>{$row['predpokladanevraceni']}</th>";
            echo "<th scope='col'>{$row['skutecnevraceni']}</th>";
            echo "<th scope='col'><a href=\"upravitvypujcky.php?action=edit&id={$row['idvypujcky']}&idknihy={$row['idknihy']}&idzakaznika={$row['idzakaznika']}&predpokladanevraceni={$row['predpokladanevraceni']}&pujceno={$row['pujcen']}&skutecnevraceni={$row['skutecnevraceni']}\">Upravit</a></th>"; //Mozna lepsi posat jenom id a pak znovu nacist z databaze?
            echo '</tr>';
        }
        echo "</table>";
    }

?>
<a href="upravitvypujcky.php?action=add">Pridat</a>    


</body>

</html>