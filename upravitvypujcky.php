<!DOCTYPE html>
<html>
<head>
    <title>Knihovna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <a href="popularita.php">Popularita autorů</a>
    <a href="administrace.php">Administrace</a>
    <br>
    <a href="autori.php">Autoři</a>
    <a href="knihy.php">Knihy</a>
    <a href="zakaznici.php">Zákazníci</a>
    <a href="vypujcky.php">Výpůjčky</a>

<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
    <input type="hidden" name="action" value="<?php echo htmlspecialchars($_GET['action']); ?>">
    ID výpůjčky: <input type="text" name="id" value="<?php echo htmlspecialchars($_GET['id']);?>" readonly><br>
    ID knihy: <input type="text" name="idknihy" value="<?php echo htmlspecialchars($_GET['idknihy']);?>"><br>
    ID zákazníka: <input type="text" name="idzakaznika" value="<?php echo htmlspecialchars($_GET['idzakaznika']);?>"><br>
    Datum půjčení: <input type="date" name="pujceno" placeholder="YYYY-MM-DD" value="<?php echo htmlspecialchars($_GET['pujceno']);?>"><br>
    Předpokládané datum vrácení : <input type="date" name="predpokladanevraceni" placeholder="YYYY-MM-DD" value="<?php echo htmlspecialchars($_GET['predpokladanevraceni']);?>"><br>
    Datum vrácení : <input type="date" name="skutecnevraceni" placeholder="YYYY-MM-DD" value="<?php echo htmlspecialchars($_GET['skutecnevraceni']);?>"><br>
    <input type="submit" value="Uložit">
</form>
<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
<input type="hidden" name="id" value="<?php echo htmlspecialchars($_GET['id']);?>" readonly><br>
<input type="hidden" name="action" value="delete">
<input type="submit" value="Smazat">
</form><?php
$dbconfig = parse_ini_file ('dbconfig.ini');
function delete()
{
    $connection = new mysqli($dbconfig['adress'],$dbconfig['usr'],$dbconfig['pwd'],$dbconfig['db']);
    $change = $connection->prepare('DELETE FROM vypujcky WHERE id=?');
    $change->bind_param('i', $_POST['id']);
    $change->execute();
    $connection->close();
}
function change()
{
    $connection = new mysqli($dbconfig['adress'],$dbconfig['usr'],$dbconfig['pwd'],$dbconfig['db']);
    $idknihy = htmlspecialchars($_POST['idknihy']);
    $idzakaznika = htmlspecialchars($_POST['idzakaznika']);
    $pujceno = dateNull(htmlspecialchars($_POST['pujceno']));
    $predpokladanevraceni = dateNull(htmlspecialchars($_POST['predpokladanevraceni']));
    $skutecnevraceni = dateNull(htmlspecialchars($_POST['skutecnevraceni']));    
    $id = htmlspecialchars($_POST['id']);    
    if ($_POST['action']=='edit' && $id!=null) {
        $change = $connection->prepare('update vypujcky set kniha=?, zakaznik=?, pujceno=?, predpokladanevraceni=?, skutecnevraceni=? where id=?');
    } else {
        $change = $connection->prepare('insert into vypujcky (kniha,zakaznik,pujceno,predpokladanevraceni,skutecnevraceni,id) values (?,?,?,?,?,?)');
    }
    echo $connection->error;    
    $change->bind_param('iisssi', $idknihy, $idzakaznika, $pujceno,$predpokladanevraceni,$skutecnevraceni , $id);
    $change->execute();
    $connection->close();
}
function dateNull($date)
{
    if (empty($date))
    {
        return NULL;
    }
    else return $date;
}
if ($_SERVER["REQUEST_METHOD"] == 'POST') {
    if ($_POST['action']=='delete') {
        delete();
    } else {
        change();
    }
    echo '<script type="text/javascript">
    window.location = "vypujcky.php"
</script>';
}
?>

</body>
</html>