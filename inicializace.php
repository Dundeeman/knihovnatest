<?php
    $dbconfig = parse_ini_file ('dbconfig.ini');
    $connection = new mysqli($dbconfig['adress'], $dbconfig['usr'], $dbconfig['pwd']);
    $self = htmlspecialchars($_SERVER['PHP_SELF']);
if ($_SERVER["REQUEST_METHOD"] == 'POST') {
    if ($_POST['action']=='createdb') {
        createDB($connection, $dbconfig);
    } elseif ($_POST['action']=='createtables') {
        createTables($connection, $dbconfig);
    }
}
    $query = "SHOW DATABASES LIKE '{$dbconfig['db']}'";
    $result = $connection->query($query);
if (!$result->fetch_array()) {
    echo "<form method='post' action='{$self}'>
    <input type='hidden' name='action' value='createdb'>        
    Neexistuje databáze <input type='submit' value='Vytvořit'>
    </form>";
} else {
    $query = "SHOW TABLES FROM {$dbconfig['db']}";
    $result = $connection->query($query);
    if (!$result->fetch_array()) {
        echo "<form method='post' action='{$self}'>
    <input type='hidden' name='action' value='createtables'>    
    Prázdná DB <input type='submit' value='Vytvořit tabulky'>
    </form>";
    }
}
$connection->close();
function createDB($connection, $dbconfig)
{
    $query = "CREATE DATABASE {$dbconfig['db']}";
    $connection->query($query);
    if ($connection->error) {
        echo $connection->error;
    } else {
        echo 'DB vytvořena';
    }
}
function createTables($connection, $dbconfig)
{
    $query = "USE {$dbconfig['db']}";
    $connection->query($query);
    $lines = file('knihovna.sql');
    $query='';
    foreach ($lines as $line) {
        if (substr($line, 0, 2)=='--' || substr($line, 0, 2)=='/*' || $line == '') {
            continue;
        }
          $query.= $line;
        if (substr(trim($query), -1)==';') {
            $connection->query($query);   
            $query='';
        }
    }
    echo 'Tabulky vytvořeny';           
}
?>