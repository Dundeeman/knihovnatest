<!DOCTYPE html>
<html>
<head>
    <title>Knihovna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <a href="popularita.php">Popularita autorů</a>
    <a href="administrace.php">Administrace</a>
    <br>
    <a href="autori.php">Autoři</a>
    <a href="knihy.php">Knihy</a>
    <a href="zakaznici.php">Zákazníci</a>
    <a href="vypujcky.php">Výpůjčky</a>

<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
    <input type="hidden" name="action" value="<?php echo htmlspecialchars($_GET['action']); ?>">
    ID : <input type="text" name="id" value="<?php echo htmlspecialchars($_GET['id']);?>" readonly><br>
    Název: <input type="text" name="nazev" value="<?php echo htmlspecialchars($_GET['nazev']);?>"><br>
    Autoři: <input type="text" name="autori" value="<?php echo htmlspecialchars($_GET['autor']);?>"> (Jednotlivé autory oddělte čárkou)<br>
    Rok vydání: <input type="date" name="vydani" value="<?php echo htmlspecialchars($_GET['vydani']);?>"><br>
  <input type="submit" value="Uložit">
</form>
<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
<input type="hidden" name="id" value="<?php echo htmlspecialchars($_GET['id']);?>" readonly><br>
<input type="hidden" name="action" value="delete">
<input type="submit" value="Smazat">
</form><?php
$dbconfig = parse_ini_file ('dbconfig.ini');
function delete()
{
    unbindAuthors($_POST['id']);
    $connection = new mysqli($dbconfig['adress'], $dbconfig['usr'], $dbconfig['pwd'], $dbconfig['db']);
    $change = $connection->prepare('delete from knihy where id=?');
    $change->bind_param('i', $_POST['id']);
    $change->execute();
    $connection->close();
}
function unbindAuthors($bookid)
{
    $connection = new mysqli($dbconfig['adress'], $dbconfig['usr'], $dbconfig['pwd'], $dbconfig['db']);
    $change = $connection->prepare('delete from autorstvi where kniha=?');
    if ($change!=null) {
        $change->bind_param('i', $bookid);
        $change->execute();
    }
    $connection->close();
}
function bindAuthors($bookname, $authorstring)
{
    $authors = explode(",", $authorstring);
    $connection = new mysqli('127.0.0.1', 'root', 'HesloKleslo', 'knihovna');
    foreach ($authors as $author) {
        if (strlen($author)>2) {
            $query = "INSERT into autorstvi (autor,kniha) select autori.id, knihy.id from autori,knihy where instr(concat(autori.jmeno,' ',autori.prijmeni),?)>0 and knihy.nazev=? LIMIT 1; ";
            $change = $connection->prepare($query);
            echo $connection->error;
            $change ->bind_param('ss', $author, $bookname);
            $change->execute();
        }
    }
    $connection->close();
}
function change()
{
    $connection = new mysqli('127.0.0.1', 'root', 'HesloKleslo', 'knihovna');
    $nazev = htmlspecialchars($_POST['nazev']);
    $autoristring = htmlspecialchars($_POST['autori']);
    $vydani = htmlspecialchars($_POST['vydani']);
    $id = htmlspecialchars($_POST['id']);
    if ($_POST['action']=='edit' && $id!=null) {
        $change = $connection->prepare('update knihy set nazev=?, rok_vydani=? where id=?');
        $change->bind_param('ssi', $nazev, $vydani, $id);
        $change->execute();
    } elseif ($_POST['action']=='add') {
        $change = $connection->prepare('insert into knihy (nazev,rok_vydani) values (?,?)');
        $change->bind_param('ss', $nazev, $vydani);
        $change->execute();
    }
    echo $connection->error;
    $connection->close();
    unbindAuthors($id);
    bindAuthors($nazev, $autoristring);
}
if ($_SERVER["REQUEST_METHOD"] == 'POST') {
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    if ($_POST['action']=='delete') {
        delete();
    } else {
        change();
    }
    echo '<script type="text/javascript">
    window.location = "knihy.php"
</script>';
}
?>

</body>
</html>